pyminer2 package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions
   pyminer2.ui
   pyminer2.workspace

Submodules
----------

pyminer2.pmappmodern module
---------------------------

.. automodule:: pyminer2.pmappmodern
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.pmutil module
----------------------

.. automodule:: pyminer2.pmutil
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2
   :members:
   :undoc-members:
   :show-inheritance:
