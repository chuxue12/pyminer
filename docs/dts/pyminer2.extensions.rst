pyminer2.extensions package
===========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.extensionlib
   pyminer2.extensions.extensions_manager
   pyminer2.extensions.package_manager
   pyminer2.extensions.test_demo

Module contents
---------------

.. automodule:: pyminer2.extensions
   :members:
   :undoc-members:
   :show-inheritance:
