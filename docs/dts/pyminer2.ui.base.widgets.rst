pyminer2.ui.base.widgets package
================================

Submodules
----------

pyminer2.ui.base.widgets.consolewidget module
---------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.consolewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.controlpanel module
--------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.controlpanel
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.flowwidget module
------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.flowwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.menu\_tool\_stat\_bars module
------------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.menu_tool_stat_bars
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.notificationwidget module
--------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.notificationwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.reportwidget module
--------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.reportwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.resources module
-----------------------------------------

.. automodule:: pyminer2.ui.base.widgets.resources
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.tablewidget module
-------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.tablewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.texteditconsolewidget module
-----------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.texteditconsolewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.treeviews module
-----------------------------------------

.. automodule:: pyminer2.ui.base.widgets.treeviews
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.base.widgets
   :members:
   :undoc-members:
   :show-inheritance:
