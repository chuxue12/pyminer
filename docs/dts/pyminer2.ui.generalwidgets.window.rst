pyminer2.ui.generalwidgets.window package
=========================================

Submodules
----------

pyminer2.ui.generalwidgets.window.applicationtest module
--------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.applicationtest
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.mainwindow\_new module
--------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.mainwindow_new
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.pmdockwidget module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.pmdockwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.pmmainwindow module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.pmmainwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.window
   :members:
   :undoc-members:
   :show-inheritance:
