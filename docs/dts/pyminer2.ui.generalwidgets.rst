pyminer2.ui.generalwidgets package
==================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.generalwidgets.basicwidgets
   pyminer2.ui.generalwidgets.browser
   pyminer2.ui.generalwidgets.containers
   pyminer2.ui.generalwidgets.demos_and_tests
   pyminer2.ui.generalwidgets.layouts
   pyminer2.ui.generalwidgets.sourcemgr
   pyminer2.ui.generalwidgets.table
   pyminer2.ui.generalwidgets.textctrls
   pyminer2.ui.generalwidgets.toolbars
   pyminer2.ui.generalwidgets.window

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets
   :members:
   :undoc-members:
   :show-inheritance:
