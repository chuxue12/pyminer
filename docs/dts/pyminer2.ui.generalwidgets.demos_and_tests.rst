pyminer2.ui.generalwidgets.demos\_and\_tests package
====================================================

Submodules
----------

pyminer2.ui.generalwidgets.demos\_and\_tests.qmenu\_demo module
---------------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.demos_and_tests.qmenu_demo
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.demos_and_tests
   :members:
   :undoc-members:
   :show-inheritance:
