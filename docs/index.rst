.. PyMiner documentation master file, created by
   sphinx-quickstart on Wed Sep  9 22:58:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyMiner's documentation!
===================================
PyMiner一款基于数据工作空间的数学工具，通过加载插件的方式实现不同的需求，用易于操作的形式完成数学计算相关工作。

User Guide
----------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user/intro
   user/tech
   user/joinus

API Reference
-----------------

.. toctree::
   :maxdepth: 30
   :caption: Contents:

   user/lib_ref.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
